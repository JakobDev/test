<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="akk">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../AboutDialog.ui" line="0"/>
        <source>About</source>
        <translation>Hall</translation>
    </message>
    <message>
        <location filename="../AboutDialog.ui" line="0"/>
        <source>A graphical Program to create and edit desktop entries</source>
        <translation>mmm</translation>
    </message>
    <message>
        <location filename="../AboutDialog.ui" line="0"/>
        <source>The Icon was created by &lt;a href=&quot;https://icon-icons.com/icon/preferences-desktop-theme/93638&quot;&gt;Papirus Development Team&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../AboutDialog.ui" line="0"/>
        <source>This Program is licensed under GPL 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../AboutDialog.ui" line="0"/>
        <source>View source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../AboutDialog.ui" line="0"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditActionDialog</name>
    <message>
        <location filename="../EditActionDialog.py" line="35"/>
        <source>Identifier empty</source>
        <translation>trtrtetrt</translation>
    </message>
    <message>
        <location filename="../EditActionDialog.py" line="35"/>
        <source>The Identifier can&apos;t be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EditActionDialog.py" line="40"/>
        <source>Identifier exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EditActionDialog.py" line="40"/>
        <source>This Identifier already exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EditActionDialog.ui" line="0"/>
        <source>Identifier:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EditActionDialog.ui" line="0"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EditActionDialog.ui" line="0"/>
        <source>Translate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EditActionDialog.ui" line="0"/>
        <source>Icon:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EditActionDialog.ui" line="0"/>
        <source>Exec:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditKeywordsTranslationDialog</name>
    <message>
        <location filename="../EditKeywordsTranslationDialog.py" line="23"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EditKeywordsTranslationDialog.py" line="24"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EditKeywordsTranslationDialog.py" line="30"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EditKeywordsTranslationDialog.py" line="49"/>
        <source>No Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EditKeywordsTranslationDialog.py" line="49"/>
        <source>You have to enter a Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EditKeywordsTranslationDialog.py" line="54"/>
        <source>Language exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EditKeywordsTranslationDialog.py" line="54"/>
        <source>This Language already exists</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Functions</name>
    <message>
        <location filename="../Functions.py" line="124"/>
        <source>{{module}} not installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Functions.py" line="124"/>
        <source>Optional module {{module}} not found. It is required to use this Feature.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Functions.py" line="135"/>
        <source>Invalid URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Functions.py" line="135"/>
        <source>The given URL is not valid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Functions.py" line="137"/>
        <source>Invalid Schema</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Functions.py" line="137"/>
        <source>Only http and https are supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Functions.py" line="139"/>
        <source>Could not connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Functions.py" line="139"/>
        <source>Could not connect to the Website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Functions.py" line="141"/>
        <source>Unknown Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Functions.py" line="141"/>
        <source>An unknown Error happened while trying to conenct to the given URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Functions.py" line="144"/>
        <source>Could not get data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Functions.py" line="144"/>
        <source>Could not get data from the URL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Language</name>
    <message>
        <location filename="../SettingsDialog.py" line="17"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SettingsDialog.py" line="18"/>
        <source>German</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListEditWidget</name>
    <message>
        <location filename="../ListEditWidget.py" line="22"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ListEditWidget.py" line="23"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ListEditWidget.py" line="24"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ListEditWidget.py" line="52"/>
        <source>Add Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ListEditWidget.py" line="52"/>
        <source>Please enter a new Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ListEditWidget.py" line="76"/>
        <location filename="../ListEditWidget.py" line="58"/>
        <source>Item in List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ListEditWidget.py" line="76"/>
        <location filename="../ListEditWidget.py" line="58"/>
        <source>This Item is already in the List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ListEditWidget.py" line="70"/>
        <source>Edit Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ListEditWidget.py" line="70"/>
        <source>Please edit the Item</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.py" line="44"/>
        <source>A list of MimeTypes that this Application can open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="45"/>
        <source>The untranslated Keywords</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="46"/>
        <source>A list of interfaces that this application implements. If you don&apos;t know what this means, you probably won&apos;t need it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="47"/>
        <source>If set, the Application is only visble when using this Desktop Environments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="48"/>
        <source>The Application is not visble when using this Desktop Environments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="132"/>
        <source>Unsaved changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="132"/>
        <source>You have unsaved changes. Do you want to save now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="142"/>
        <source>Untitled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="155"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="170"/>
        <source>No templates found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="193"/>
        <source>No recent files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="206"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="244"/>
        <source>Open URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="244"/>
        <source>Please enter the URL to the Desktop Entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="273"/>
        <source>Add a Categorie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="273"/>
        <source>Please select a Categorie from the list below</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="277"/>
        <source>Categorie already added</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="277"/>
        <source>You can&apos;t add the same Categorie twice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="319"/>
        <source>Delete Keywords translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="319"/>
        <source>Are you really want to delete {{language}}?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="336"/>
        <source>Add Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="336"/>
        <source>Please enter the identifier for the new Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="342"/>
        <source>Identifier exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="342"/>
        <source>This Identifier already exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="366"/>
        <source>Delete Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="366"/>
        <source>Are you really want to delete {{identifier}}?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.py" line="391"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="410"/>
        <source>Invalid Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="410"/>
        <source>{{key}} is not a valid custom Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="414"/>
        <source>Everything valid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="414"/>
        <source>No issues found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="451"/>
        <location filename="../MainWindow.py" line="426"/>
        <source>Error loading Desktop Entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="451"/>
        <location filename="../MainWindow.py" line="426"/>
        <source>This Desktop Entry couldn&apos;t be loaded. Make sure, it is in the right format.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Translate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>GenericName:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Comment:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Icon:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Exec:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>TryExec:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>StartupWMClass:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>URL:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Don&apos;t display in Menu (NoDisplay)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Hide Desktop Entry (Hidden)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Program has a single main window (SingleMainWindow)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Run on a more powerful discrete GPU if available (PrefersNonDefaultGPU)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Send a Message when started (StartupNotify)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Run in a Terminal (Terminal)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Can be activated with D-Bus (DBusActivatable)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Categories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>MimeType</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Keywords</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Untranslated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Translated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Implements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>OnlyShowIn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>NotShowIn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Open recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>New (from Template)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Save as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Validate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Desktop Entry Specification</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Manage templates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Open from URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Show Welcome Dialog</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ManageTemplatesWindow</name>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="67"/>
        <location filename="../ManageTemplatesWindow.py" line="46"/>
        <source>Enter name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="46"/>
        <source>This will save your current document as template. Please enter a name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="73"/>
        <location filename="../ManageTemplatesWindow.py" line="52"/>
        <source>Name exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="73"/>
        <location filename="../ManageTemplatesWindow.py" line="52"/>
        <source>There is already a template with this name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="67"/>
        <source>Please enter the new name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="99"/>
        <location filename="../ManageTemplatesWindow.py" line="82"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="82"/>
        <source>A error occurred while renaming</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="92"/>
        <source>Delete {{name}}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="92"/>
        <source>Are you sure you want to delete {{name}}?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="99"/>
        <source>A error occurred while deleting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.ui" line="0"/>
        <source>Manage templates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.ui" line="0"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.ui" line="0"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.ui" line="0"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.ui" line="0"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PreviewDialog</name>
    <message>
        <location filename="../PreviewDialog.ui" line="0"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PreviewDialog.ui" line="0"/>
        <source>Preview of your desktop entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PreviewDialog.ui" line="0"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PreviewDialog.ui" line="0"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../SettingsDialog.py" line="31"/>
        <source>System language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SettingsDialog.py" line="43"/>
        <source>Nothing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SettingsDialog.py" line="44"/>
        <source>Filename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SettingsDialog.py" line="45"/>
        <source>Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="0"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="0"/>
        <source>Language:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="0"/>
        <source>(needs restart)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="0"/>
        <source>Length of recent files list:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="0"/>
        <source>Window title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="0"/>
        <source>Ask before closing unsaved File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="0"/>
        <source>Show in Title if File is edited</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="0"/>
        <source>Add comment to Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="0"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="0"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="0"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TranslateDialog</name>
    <message>
        <location filename="../TranslateDialog.ui" line="0"/>
        <location filename="../TranslateDialog.py" line="37"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateDialog.py" line="53"/>
        <source>No Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateDialog.py" line="53"/>
        <source>You had no Language for at least one Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateDialog.py" line="57"/>
        <source>Language double</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateDialog.py" line="57"/>
        <source>{{Language}} appears twice or more times in the table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateDialog.ui" line="0"/>
        <source>Translate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateDialog.ui" line="0"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateDialog.ui" line="0"/>
        <source>Translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateDialog.ui" line="0"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateDialog.ui" line="0"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateDialog.ui" line="0"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ValidationDialog</name>
    <message>
        <location filename="../ValidationDialog.py" line="23"/>
        <source>desktop-file-validate was not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ValidationDialog.ui" line="0"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ValidationDialog.ui" line="0"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WelcomeDialog</name>
    <message>
        <location filename="../WelcomeDialog.ui" line="0"/>
        <source>Welcome</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../WelcomeDialog.ui" line="0"/>
        <source>Welcome to jdDesktopEntryEdit!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../WelcomeDialog.ui" line="0"/>
        <source>This Program allows you to create and edit Desktop Entries according to the Freedesktop Specification.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../WelcomeDialog.ui" line="0"/>
        <source>Unlike other Programs, which try to make things easy by implementing only the main parts, the Goal of jdDesktopEntryEdit is to support the full specification. Thefore, it is expected to read the Specification before using this Program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../WelcomeDialog.ui" line="0"/>
        <source>You can view the Specification at ?&gt;Destop Entry Specification.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../WelcomeDialog.ui" line="0"/>
        <source>You can validate your Desktop Entry at Tools&gt;Validate.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../WelcomeDialog.ui" line="0"/>
        <source>Show this Dialog on Startup</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
