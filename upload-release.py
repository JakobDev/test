#!/usr/bin/env python
import subprocess
import argparse
import requests
import os


def check_env(name: str) -> None:
    if os.getenv(name) is None:
        exit(f"Environment variable {name} is not set")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--filename", help="Sets a custom filename")
    parser.add_argument("file", help="The path to the file")
    args = parser.parse_args()

    if not os.path.isfile(args.file):
        exit(f"{args.file} was not found")

    check_env("CODEBERG_API_TOKEN")
    check_env("CI_REPO")

    repo =  os.getenv("CI_REPO")

    release_id = 1094870

    headers = {
        "accept": "application/json",
        "Authorization": "token " + os.getenv("CODEBERG_API_TOKEN")
    } 

    files = [
       ("attachment", (args.filename or os.path.basename(args.file), open(args.file, "rb"),"application/gzip")),
    ]

    r = requests.post(f"https://codeberg.org/api/v1/repos/{repo}/releases/{release_id}/assets", headers=headers, files=files)

    if not r.ok:
        exit(r.json())


if __name__ == "__main__":
    main()
